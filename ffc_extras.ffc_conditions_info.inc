<?php

/**
 * Implements hook_ffc_conditions_info().
 */
function ffc_extras_ffc_conditions_info() {

  $conditions = array(
    'hide_if_empty' => array(
      'title' => t('Hide if target field is empty'),
    ),
    'hide_no_string' => array(
      'title' => t('Hide when target field does not contain a string'),
    ),
    'hide_if_string' => array(
      'title' => t('Hide when target field contains a string'),
    ),
  );

  return $conditions;
}

/**
 * Implements ffc_condition_form_CONDITION().
 *
 * Field formatter conditional form.
 *
 * Present the hide when empty form.
 */
function ffc_condition_form_hide_if_empty($context, $configuration) {
  $form = array();

  $form['target'] = array(
    '#type' => 'select',
    '#title' => t('Select target field'),
    '#options' => ffc_get_condition_fields($context),
    '#default_value' => isset($configuration['target']) ? $configuration['target'] : '',
  );

  return $form;
}

/**
 * Implements ffc_condition_form_CONDITION().
 *
 * Field formatter conditional form.
 *
 * Present the hide if string not found form.
 */
function ffc_condition_form_hide_no_string($context, $configuration) {
  $form = array();

  $form['target'] = array(
    '#type' => 'select',
    '#title' => t('Select target field'),
    '#options' => ffc_get_condition_fields($context),
    '#default_value' => isset($configuration['target']) ? $configuration['target'] : '',
  );
  $form['string'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter target string'),
    '#default_value' => isset($configuration['string']) ? $configuration['string'] : '',
  );

  return $form;
}

/**
 * Implements ffc_condition_form_CONDITION().
 *
 * Field formatter conditional form.
 *
 * Present the hide when string is present form.
 */
function ffc_condition_form_hide_if_string($context, $configuration) {
  $form = array();

  $form['target'] = array(
    '#type' => 'select',
    '#title' => t('Select target field'),
    '#options' => ffc_get_condition_fields($context),
    '#default_value' => isset($configuration['target']) ? $configuration['target'] : '',
  );
  $form['string'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter target string'),
    '#default_value' => isset($configuration['string']) ? $configuration['string'] : '',
  );

  return $form;
}